# Drone setup instructions #

R.D. McAllister, March 2022

Tutorial documents can be found at: <https://bitcraze.io>. In this document, we specialize these intructions to the labratory environment at UCSB. 

## Drone setup (from scratch) ##

Assembly intructions can be found at <https://www.bitcraze.io/documentation/tutorials/getting-started-with-crazyflie-2-x/>. NOTE: Pay attention to the propeller placement. The two motors across from each other spin the same direction (clockwise or counter clockwise), while the other two motors spin the other direction. The propellers are directional and must be placed accordingly. This cancels the torque of the spinning motors. Test that the crazyflie turns on and completes the startup check without errors before proceeding. 

The crazyflie client can be downloaded through `python3` (>=3.7) and `pip`. On Windows run in the command prompt: `pip3 install --upgrade pip` to upgrade pip and then `pip3 install cfclient` to download the crazyflie software. Additional installation instructions can be found at <https://www.bitcraze.io/documentation/repository/crazyflie-clients-python/master/installation/install/>. At this time, we also recommend installing `git` (if not installed already intalled) and cloning the group repo: `git clone https://rdmcallister@bitbucket.org/rawlings-group/crazyflie_ucsb.git` on the laptop to be used for the drone experiments. On Windows machines, we recommend installing and using visual studie code to display/edit/run the python scripts. 

If the crazyflie radio dongle (Crazyradio) has never been plugged into the machine before, you will need to download the correct driver. To do this, first download Zadig: <http://zadig.akeo.ie/>. Then plug in the Crazyradio in at USB port. Once you close out of any initial windows (including error messages if they appear), launch Zadig. You should see the device listed as "Crazyradio USB Dongle" and should select the libsub-win32 driver. Then press "Install Driver". Wait for the installation to finish and close of the windows. The Crazyradio driver should be installed/configured. Additional instructions can be found at <https://www.bitcraze.io/documentation/repository/crazyradio-firmware/master/building/usbwindows/>.

Then you can open the cfclient gui by running `python3 -m cfclient.gui` in the command prompt. Turn on the drone and and test that the cfclient can connect to the drone through the gui. Press "Scan", select the crazyflie URI, and then press "Connect" in the top left corner of the gui to connect. If connection is sucesful, you should see the pitch/roll/yaw values respond to movements of the drone and the battery level should be displayed. 

To update the crazyflie firmware go to the menu *Crazyflie->Bootloader*, connect to the crazyflie in this dialog (if not already connected), select the latest release from the drop down menu, click "Program" and wait for the update to finish. Do not touch or restart the crazyflie until all flashing is done and you see "status:idle" at the bottom of the gui. 

## Loco positioning anchors setup (from scratch) ##

More more details and pictures see: <https://www.bitcraze.io/documentation/tutorials/getting-started-with-loco-positioning-system/#anchor-positions>.

The loco positioning expansion deck must first be attached to the top of the crazyflie drone. See <https://www.bitcraze.io/documentation/tutorials/getting-started-with-expansion-decks/> for pictures and more details. The manufacture suggests replacing the battery holder with the explansion deck, but we have elected to keep the battery holder below the expansion deck to ensure that bowing/bending of the expansion deck does not occur from pressing directly on the battery. Future users may use their own judgement. 

Next, we need to configure the eight (8) loco positioning anchors/nodes (arduinos). First download and run the "Loco Positioning Configuration tool" or lpstool: <https://github.com/bitcraze/lps-tools/releases>. Note that windows firewalls may block the installation and you need to overide them. Also, download the latest loco positioning node firmware at <https://github.com/bitcraze/lps-node-firmware/releases>. 

To update the firmware, first start/connect the lps node in DFU mode. Keep the DFU button pressed while connecting the node to the computer via USB. For Windows, the node will not be recognized the first time. Use Zadig (as discussed previously) to install the USB driver (libsub-win32). In DFU mode, the node will be displayed as "STM32 BOOTLOADER" in Zadig. In the lpstool, click browse and select the loco positioning node firmware that you just downloaded. Click update. When the update is complete press reset on the positioning node. 

To configure the nodes, first connect the node via USB without pressing any buttons. Select an appropriate ID in the lpstool. Choose "Anchor (TWR)" mode and click apply. Repeat for all 8 nodes with different IDs for the nodes ranging from 0-7. Mark each node with a piece of tape to display the node ID. At this point, the nodes are now *anchors*. 

Before placing the anchors, test that each anchor can connect to the drone. Open up the cfclient gui by running `python3 -m cfclient.gui` in the command prompt. Power up the anchors (preferable with wall power supplies, as they can quickly drain 9V batteries). Turn on the drone and connect to the drone through the cfclient gui. Once connected, open the loco positioning tab in the gui (you may need to check it in the menu View->Tabs->Loco Positioning Tab to make it visible). This tab will show the anchors that are connected to the drone as green and red otherwise. NOTE: the anchors connect to the drone and not directly to the crazyradio dongle. Therefore, the anchors will not "connect" unless the drone is also on and connected to the gui. 

Place anchors in the room to form a cube. In the undergraduate lab, 4 anchors are placed on the permanent anchor mounts suspened in the air while the remaining 4 anchors are placed on small anchor stands at specific locations on the group marked by tape. Run power supplies to all anchors and plug them in. Again, test that the anchors can connect to the crazyflie with the procedure detailed in the previous paragraph. 

The next step is to enter the anchors positions in space. These positions are relative and the corrdinate system can be setup however the user desires. The current standard is to treat the center of the cube at floor level as (x,y,z) = (0,0,0). So the z coordinate is the distance of the drone (in meters) from the ground and always positive. Through the gui, with the drone and anchors connected, click the "Configure positions" button in loco positioning tab to bring up a new window. Click "Get from anchors" to pull the current set of positiions from the anchors. Adjust the positions as needed (measurements are in meters). Any changes are displayed in red. Once the adjustments are complete, press "Write to anchors" to update the anchors with the new positions. All of the cells should be displayed in green. Take care with the measurements as they are directly related to the quality of the position estimates from the drone in flight. The (crudely) measured positions for the current positioning node setup can be found in the group git repo on bitbucket: crazyflie_ucsb/7_30_2020.yaml. 

To verify the anchor positions, select "Anchor indentification" in the "Graph settings" box of the loco positioning tab. Move the crazyflie close to an anchor and verify that the correct anchor ID is indentified in the graph provided in the gui. Next, switch back to "Position estimate" in the "Graph settings" and move the crazyflie around to see if the graphs and position estimates correspond to the physical movement. 

There are additional configuration steps if we want to fly more than one crazyflie at a time. We leave that setup and the corresponding instructions to an ambitious future graduate student. 

At this point, the Crazyflie and loco position system is setup and configured. Happy flying! 




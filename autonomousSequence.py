# -*- coding: utf-8 -*-
#
#     ||          ____  _ __
#  +------+      / __ )(_) /_______________ _____  ___
#  | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
#  +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
#   ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
#
#  Copyright (C) 2016 Bitcraze AB
#
#  Crazyflie Nano Quadcopter Client
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA  02110-1301, USA.
"""
Simple example that connects to one crazyflie (check the address at the top
and update it to your crazyflie address) and send a sequence of setpoints,
one every 5 seconds.

This example is intended to work with the Loco Positioning System in TWR TOA
mode. It aims at documenting how to set the Crazyflie in position control mode
and how to send setpoints.
"""
import time
import scipy.io as sio

import cflib.crtp
from cflib.crazyflie import Crazyflie
from cflib.crazyflie.log import LogConfig
from cflib.crazyflie.syncCrazyflie import SyncCrazyflie
from cflib.crazyflie.syncLogger import SyncLogger

# URI to the Crazyflie to connect to
uri = 'radio://0/80/2M'
load_mat = "nonlineardroneMPC-skuntz.mat"
save_mat = "nonlineardroneMPC-skuntz_data.mat"

# Change the sequence according to your setup
#             x    y    z  YAW
#sequence = [
#    (0, -0.5, 0.5, 0),
#    (0, -0.5, 1.0, 0),
#    (0, -0.5, 1.0, 0),
#    (0, -0.5, 1.0, 0),
#    (0, -0.5, 1.0, 0),
#    (0, -0.5, 0.5, 0),
#    (0, -0.5, 0, 0),
#]

data = sio.loadmat(load_mat)
sequence = data["seq"]

def wait_for_position_estimator(scf):
    print('Waiting for estimator to find position...')

    log_config = LogConfig(name='Kalman Variance', period_in_ms=500)
    log_config.add_variable('kalman.varPX', 'float')
    log_config.add_variable('kalman.varPY', 'float')
    log_config.add_variable('kalman.varPZ', 'float')

    var_y_history = [1000] * 10
    var_x_history = [1000] * 10
    var_z_history = [1000] * 10

    threshold = 0.001

    with SyncLogger(scf, log_config) as logger:
        for log_entry in logger:
            data = log_entry[1]

            var_x_history.append(data['kalman.varPX'])
            var_x_history.pop(0)
            var_y_history.append(data['kalman.varPY'])
            var_y_history.pop(0)
            var_z_history.append(data['kalman.varPZ'])
            var_z_history.pop(0)

            min_x = min(var_x_history)
            max_x = max(var_x_history)
            min_y = min(var_y_history)
            max_y = max(var_y_history)
            min_z = min(var_z_history)
            max_z = max(var_z_history)

            # print("{} {} {}".
            #       format(max_x - min_x, max_y - min_y, max_z - min_z))

            if (max_x - min_x) < threshold and (
                    max_y - min_y) < threshold and (
                    max_z - min_z) < threshold:
                break


def reset_estimator(scf):
    cf = scf.cf
    cf.param.set_value('kalman.resetEstimation', '1')
    time.sleep(0.1)
    cf.param.set_value('kalman.resetEstimation', '0')

    wait_for_position_estimator(cf)

## Data logging routines

datalist = {}
kalman = ['kalman.stateX','kalman.stateY','kalman.stateZ']
kalman_data = {'t':[]}
for key in kalman:
    newkey = key.split('.')
    kalman_data[newkey[1]] = []

stab = ['stabilizer.roll','stabilizer.pitch','stabilizer.yaw','stabilizer.thrust']
stab_data = {'t':[]}
for key in stab:
    newkey = key.split('.')
    stab_data[newkey[1]] = []

pwm = ['pwm.m1_pwm','pwm.m2_pwm','pwm.m3_pwm','pwm.m4_pwm']
pwm_data = {'t':[]}
for key in pwm:
    newkey = key.split('.')
    pwm_data[newkey[1]] = []

gyro = ['gyro.x','gyro.y','gyro.z']
gyro_data = {'t':[]}
for key in gyro:
    newkey = key.split('.')
    gyro_data[newkey[1]] = []

acc = ['acc.x','acc.y','acc.z']
acc_data = {'t':[]}
for key in acc:
    newkey = key.split('.')
    acc_data[newkey[1]] = []

def default_callback(timestamp, data, logconf,data_struct,datalist):
    data_struct['t'].append(timestamp)
    for key in datalist:
        newkey = key.split('.')
        data_struct[newkey[1]].append(data[key])
    
def position_callback(timestamp,data,logconf):
    default_callback(timestamp,data,logconf,kalman_data,kalman)
    
def stab_callback(timestamp, data, logconf):
    default_callback(timestamp,data,logconf,stab_data,stab)

def pwm_callback(timestamp, data, logconf):
    default_callback(timestamp, data, logconf, pwm_data, pwm)

def gyro_callback(timestamp, data, logconf):
    default_callback(timestamp, data, logconf, gyro_data, gyro)

def acc_callback(timestamp, data, logconf):
    default_callback(timestamp, data, logconf, acc_data, acc)

def start_position_printing(scf):
    log_conf = LogConfig(name='Position', period_in_ms=10)
    log_conf.add_variable('kalman.stateX', 'float')
    log_conf.add_variable('kalman.stateY', 'float')
    log_conf.add_variable('kalman.stateZ', 'float')

    scf.cf.log.add_config(log_conf)
    log_conf.data_received_cb.add_callback(position_callback)
    log_conf.start()

def start_stab_printing(scf):
    lg_stab = LogConfig(name='Stabilizer', period_in_ms=10)
    lg_stab.add_variable('stabilizer.roll', 'float')
    lg_stab.add_variable('stabilizer.pitch', 'float')
    lg_stab.add_variable('stabilizer.yaw', 'float')
    lg_stab.add_variable('stabilizer.thrust', 'uint16_t')

    scf.cf.log.add_config(lg_stab)
    lg_stab.data_received_cb.add_callback(stab_callback)
    lg_stab.start()

def start_pwm_printing(scf):
    lg_pwm = LogConfig(name='PWM', period_in_ms=10)
    lg_pwm.add_variable('pwm.m1_pwm', 'uint32_t')
    lg_pwm.add_variable('pwm.m2_pwm', 'uint32_t')
    lg_pwm.add_variable('pwm.m3_pwm', 'uint32_t')
    lg_pwm.add_variable('pwm.m4_pwm', 'uint32_t')

    scf.cf.log.add_config(lg_pwm)
    lg_pwm.data_received_cb.add_callback(pwm_callback)
    lg_pwm.start()

def start_gyro_printing(scf):
    lg_gyro = LogConfig(name="gyro", period_in_ms=10)
    lg_gyro.add_variable('gyro.x', 'float')
    lg_gyro.add_variable('gyro.y', 'float')
    lg_gyro.add_variable('gyro.z', 'float')

    scf.cf.log.add_config(lg_gyro)
    lg_gyro.data_received_cb.add_callback(gyro_callback)
    lg_gyro.start()

def start_acc_printing(scf):
    lg_acc = LogConfig(name="acc", period_in_ms=10)
    lg_acc.add_variable('acc.x','float')
    lg_acc.add_variable('acc.y','float')
    lg_acc.add_variable('acc.z','float')

    scf.cf.log.add_config(lg_acc)
    lg_acc.data_received_cb.add_callback(acc_callback)
    lg_acc.start()


def take_off(cf, position, t):
    t = int(t/0.1)
    for i in range(t):
        cf.commander.send_position_setpoint(position[0],
                                            position[1],
                                            position[2],
                                            position[3])
        time.sleep(0.1)

def land(cf, position, t):
    t = int(t/0.2)
    for i in range(t):
        cf.commander.send_position_setpoint(position[0],
                                            position[1],
                                            position[2]/2.0,
                                            position[3])
        time.sleep(0.1)
    for i in range(t):
        cf.commander.send_position_setpoint(position[0],
                                            position[1],
                                            0.0,
                                            position[3])
        time.sleep(0.1)

def run_sequence(scf, sequence):
    cf = scf.cf

    take_off(cf, sequence[0], 3.0)

    for position in sequence:
        print('Setting position {}'.format(position))
        cf.commander.send_position_setpoint(position[0],
                                            position[1],
                                            position[2],
                                            position[3])
        time.sleep(0.1)

    land(cf, sequence[-1], 3.0)
    cf.commander.send_stop_setpoint()
    # Make sure that the last packet leaves before the link is closed
    # since the message queue is not flushed before closing
    time.sleep(0.1)


if __name__ == '__main__':
    cflib.crtp.init_drivers(enable_debug_driver=False)

    with SyncCrazyflie(uri, cf=Crazyflie(rw_cache='./cache')) as scf:
        reset_estimator(scf)
        start_position_printing(scf)
        start_stab_printing(scf)
        start_pwm_printing(scf)
        start_gyro_printing(scf)
        start_acc_printing(scf)
        run_sequence(scf, sequence)

    sio.savemat(save_mat,{'kalman':kalman_data,'stab':stab_data,'pwm':pwm_data,'gyro':gyro_data,'acc':acc_data})

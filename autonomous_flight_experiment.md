# Autonomous Flight Experiment #

R.D. McAllister, March, 2022

We detail the setup and excecution of an autonomous flight experiment for the crazyflie drone and loco positioning system. This guide assumes that all steps in the "setup_from_scratch.md" file have already been completed at an earlier date. We designed this guide for the experiment conducted in the ChE152B: Advanced procedd control course, but keep the descriptions general enough to remain flexible with alterations to the experiment. 

## Setup ## 

Remove: 
	-(4) power supplies (wall outlet)
	-(4) loco positioning anchors and their stands
	- crazyflie drone
	- battery charger and spare battery
	- radio USB dongle
	- laptop
from the drawer in the undergraduate lab labeled "Koty's drone." Ask the lab manager if the name/drawer has changed. 

Place loco positioning anchors and their stands on top of the spots on the ground marked by tape. Make sure the number of the back of the positioning anchor matches the number on the ground. Plug in and power up each of these anchors with the power supplies. Turn on the loco positioning anchors permanently mounted on the ceiling by turning on the power strips mounted on the ceiling on both sides of the room. This can be done with a ladder or (with some dexterity) a long stick/ruler. 

Power up the laptop and plug in the radio USB dongle. In the command prompt, run `python3 -m cfclient.gui` to open the client gui. Power up the crazyflie. In the client gui, click "Scan" to locate the radio/crazyflie and then click "Connect". Go to the loco positioning tab in the gui and check that all 8 loco positioning anchors are green/connected. Also, check that the battery is charged. Anything below 1/3 of full charge may produce poor flight results. 

To verify the anchor positions, select "Anchor indentification" in the "Graph settings" box of the loco positioning tab. Move the crazyflie close to an anchor and verify that the correct anchor ID is indentified in the graph provided in the gui. Next, switch back to "Position estimate" in the "Graph settings" and move the crazyflie around to see if the graphs and position estimates correspond to the physical movement. 

## Running the experiment ##

The autonomous sequence experiment is run via the script: `autonomousSequence.py`. Use your favorite editor to open the script and edit it accordingly (the python IDLE or visual studio code both work fine). 

The sequence/trajectory to be followed is set by the variable `sequence`. This variable can be set manually in the script or can be set by importing a properly formated `.mat` file. The sequence must be a 2-d interable object with a trajetory of (x,y,z,yaw) setpoints with 0.1 second time steps (10 Hz). After running the script, the crazyflie will execute a take-off sequence to get from the ground to the first point in the sequence, then execute the specified trajectory, and then perform a landing sequence at the same x-y coordinate as the final point in the specified trajectory. The data (including position, stabilizer, pwm, gyroscope, and accelerometer data) is then saved to a file. To load a run a sequence from a `.mat` file and then save the data in a `.mat` file, change the `load_mat` and `save_mat` variable names accordingly. 

Once the script is setup, we can run the experiment. Place the drone on the ground as close to the first x-y position in the specified trajectory as possible. Make sure that the drone's nose (front), as indicated by a pertruding piece of metal on one side of the center microprocessor, is facing in the positive x direction (+x). Currently, this means the drone should face the right side of the room if looking at the drone flight area from the lab doorway (drone should face towards the loading bay door). NOTE: if the drone is not placed in the +x direction, the yaw estimate will be poor and the drone will crash before correcting the initialization error. Turn on the drone and move out of the flight zone (cube formed by the 8 positioning anchors). 

Then, from the command line (or any python IDE), run `autonomousSequence.py`. For example, navigate the command prompt to the working directory with the script and `.mat` file and run: `python3 autonomousSequence.py`. The script will first connect, wait for a good position estimate, then execute the experiment, and finally save the flight data to the specified `.mat` file. 

## Tips ##
- Make sure the spare batter is being charged whenever possible. Continuous flight can drone the battery quickly and fully charging the battery can take more than 30 minutes. 
- Keep the drone in the line-of-sight of the radio dongle attached to the laptop to avoid loosing connection.
- Only fly the drone within the interior of the cube formed by the positioning anchors to ensure the position estimates are accurate. 
- Do not attempt to fly the drone with .25 meters of the ground (excluding take-off and landing). The "ground effect" and the fact that this region is ad the edge of the cube of positioning anchors makes the drone difficult to control and the positiion estimate poor. 
- If you notice that the drone cannot maintain altitutde or appears slugish, try changing the battery. 
- There are multiple replacements parts for every piece of hardware that does not include a microprocessor: motors, propellers, motor mounts, and batteries.
- There is at least one replacement for every piece of hardware that does include a microprocessor, including an additional radio dongle, loco positioning node, and an entirely new drone. 
- Turn off the drone inbetween runs to save battery and to allow the position estimator to reset when you move/restart the drone at a new position.
